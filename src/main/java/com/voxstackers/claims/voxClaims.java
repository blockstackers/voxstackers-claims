package com.voxstackers.claims;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginLogger;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.Bukkit;

import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Random;

import com.voxstackers.claims.executors.*;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.bukkit.WGBukkit;

public final class voxClaims extends JavaPlugin {
    public  Logger  log = null;

    public void voxClaims() {
    }

    @Override
    public void onEnable() {
        this.log = this.getLogger();
        this.saveDefaultConfig();

        this.getCommand("claim").setExecutor(new ClaimExecutor(this));
    }

    @Override
    public void onDisable() {
    }

    public static WorldGuardPlugin getWG() {
        return WGBukkit.getPlugin();
    }

    public RegionManager getRegionManager(World world) {
        return WGBukkit.getRegionManager(world);
    }

    public void sendMessage(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    public PlayerClaim loadClaim(Player p) {
        PlayerClaim c = new PlayerClaim(p, this);
        return c;
    }
}
