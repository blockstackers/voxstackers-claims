package com.voxstackers.claims;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.UUID;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.BukkitPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.InvalidFlagFormat;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.bukkit.WGBukkit;

public class PlayerClaim {
    private final Player player;
    private final voxClaims plugin;
    private YamlConfigAccessor config;

    public PlayerClaim(Player p, voxClaims plugin) {
        this.player = p;
        this.plugin = plugin;
        this.config = new YamlConfigAccessor(plugin, "claims/" + p.getUniqueId().toString() + ".yml");
    }

    private void log(String message) {
        plugin.log.info("[PlayerClaim:" + player.getName() + "/" + player.getUniqueId().toString() + "/" + player.getLocation().getWorld().getName() + "]: " + message);
    }

    public String getName(World w) {
        String name = "claim_" + player.getUniqueId().toString() + "_" + w.getName().toLowerCase();
        return name;
    }

    public boolean existsInWorld(World w) {
        RegionManager rm = plugin.getRegionManager(w);
        return rm.hasRegion(getName(w));
    }

    public Flag<?> fuzzyMatchFlag(String id) {
        for (Flag<?> flag : DefaultFlag.getFlags()) {
            if (flag.getName().replace("-", "").equalsIgnoreCase(id.replace("-", ""))) {
                return flag;
            }
        }
        return null;
    }

    public boolean setRegionFlag(CommandSender s, String f, String value) {
        Flag<?> flag = fuzzyMatchFlag(f);
        if(flag != null) {
            boolean result = setRegionFlag(s, flag, value);
            if(result) {
                saveRegion(((Player)s).getLocation().getWorld());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void addMember(CommandSender s, String player) {
        Player pObject = Bukkit.getPlayer(player);
        getClaimRegion().getMembers().addPlayer(pObject.getUniqueId());
        saveRegion(((Player)s).getLocation().getWorld());
    }

    public void removeMember(CommandSender s, String player) {
        Player pObject = Bukkit.getPlayer(player);
        getClaimRegion().getMembers().removePlayer(pObject.getUniqueId());
        saveRegion(((Player)s).getLocation().getWorld());
    }

    public boolean setRegionFlag(CommandSender s, Flag f, String value) {
        log("setting region flag with sender: " + s + " flag " + f + " value " + value);
        try {
            getClaimRegion().setFlag(f, f.parseInput(plugin.getWG(), s, value));
            return true;
        } catch(InvalidFlagFormat ex) {
            log("exception invalid flag format: " + ex);
            return false;
        } catch(Exception ex) {
            log("exception: " + ex);
            ex.printStackTrace();
            return false;
        }
    }

    public Map<Flag<?>, Object> getFlags() {
        RegionManager rm = plugin.getRegionManager(player.getLocation().getWorld());
        ProtectedRegion r = rm.getRegion(getName(player.getLocation().getWorld()));
        return r.getFlags();
    }

    public ProtectedRegion getClaimRegion() {
        RegionManager rm = plugin.getRegionManager(player.getLocation().getWorld());
        ProtectedRegion r = rm.getRegion(getName(player.getLocation().getWorld()));
        return r;
    }

    public void removeFromWorld(World w) {
        RegionManager rm = plugin.getRegionManager(w);
        rm.removeRegion(getName(w));

        saveRegion(w);

        config.getConfig().set(w.getName() + ".claim", false);
        config.getConfig().set(w.getName() + ".center.x", null);
        config.getConfig().set(w.getName() + ".center.z", null);
        config.getConfig().set(w.getName() + ".region", null);
        config.saveConfig();
    }

    public String getCenterCoordinates(World w) {
        int x = config.getConfig().getInt(w.getName() + ".center.x");
        int z = config.getConfig().getInt(w.getName() + ".center.z");

        return "X: " + x + ", Z: " + z;
    }

    public boolean createInWorld(World w, int centerX, int centerZ) {
        Location centerLoc = new Location(w, centerX + 0.0D, 64, centerZ + 0.0D);
        return createInWorld(centerLoc);
    }

    public void saveRegion(World w) {
        RegionManager rm = plugin.getRegionManager(w);
        try {
            rm.save();
        } catch(Exception ex) {
            log("error saving: " + ex);
        }
    }

    public boolean createInWorld(Location centerLoc) {
        RegionManager rm = plugin.getRegionManager(centerLoc.getWorld());

        int xsize = plugin.getConfig().getInt("claim.size.x");
        int zsize = plugin.getConfig().getInt("claim.size.z");

        int xwithborder = xsize + plugin.getConfig().getInt("claim.border");
        int zwithborder = zsize + plugin.getConfig().getInt("claim.border");

        String claimName  = getName(centerLoc.getWorld());
        String borderName = claimName + "_border";

        BlockVector b_tl = new BlockVector(centerLoc.getX() - 1 - xwithborder, 0, centerLoc.getZ() - 1 - zwithborder);
        BlockVector b_br = new BlockVector(centerLoc.getX() + 1 + xwithborder, 255, centerLoc.getZ() + 1 + zwithborder);

        BlockVector c_tl = new BlockVector(centerLoc.getX() - 1 - xsize, 0, centerLoc.getZ() - 1 - zsize);
        BlockVector c_br = new BlockVector(centerLoc.getX() + 1 + xsize, 255, centerLoc.getZ() + 1 + zsize);

        World w = centerLoc.getWorld();

        log("creating new claim centered on " + centerLoc + " with name: " + getName(centerLoc.getWorld()));

        ProtectedRegion border_region = new ProtectedCuboidRegion(borderName, b_tl, b_br);
        ProtectedRegion claim_region = new ProtectedCuboidRegion(claimName, c_tl, c_br);

        claim_region.setPriority(20);

        if(rm.overlapsUnownedRegion(border_region, new BukkitPlayer(plugin.getWG(), player))) return false;

        DefaultDomain owners = new DefaultDomain();

        owners.addPlayer(player.getUniqueId()); 
        claim_region.setOwners(owners);

        rm.addRegion(claim_region); 

        saveRegion(centerLoc.getWorld());

        for(Flag f : DefaultFlag.getFlags()) {
            // find it's default value if we got one; the enter/exit messages are handled separately
            if(plugin.getConfig().isSet("flags.default." + f.getName())) {
                boolean b = plugin.getConfig().getBoolean("flags.default." + f.getName());
                if(b == true) {
                    setRegionFlag((CommandSender)player, f, "allow");
                } else {
                    setRegionFlag((CommandSender)player, f, "deny");
                }
            }
        }
        setRegionFlag((CommandSender)player, "greeting", ChatColor.GRAY + "Entering: " + ChatColor.WHITE + "Claim of " + player.getName());
        setRegionFlag((CommandSender)player, "farewell", ChatColor.GRAY + "Leaving: " + ChatColor.WHITE + "Claim of " + player.getName());

        saveRegion(centerLoc.getWorld());

        config.getConfig().set(w.getName() + ".claim", true);
        config.getConfig().set(w.getName() + ".center.x", centerLoc.getX());
        config.getConfig().set(w.getName() + ".center.z", centerLoc.getZ());
        config.getConfig().set(w.getName() + ".region", getName(w));
        config.saveConfig(); 

        return true;
    }
}
