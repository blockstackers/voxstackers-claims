package com.voxstackers.claims.executors;

import com.voxstackers.claims.voxClaims;
import com.voxstackers.claims.PlayerClaim;
import com.voxstackers.claims.YamlConfigAccessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Random;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.Command;
import org.bukkit.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.Chunk;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.BukkitPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;

import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.InvalidFlagFormat;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class ClaimExecutor implements CommandExecutor {
    private voxClaims plugin;

    public ClaimExecutor(voxClaims plugin) {
        this.plugin = plugin;
    }

    private void log(String message) {
        plugin.log.info("[ClaimExecutor]: " + message);
    }

    private boolean noPermission(CommandSender sender) {
        plugin.sendMessage(sender, "&cYou do not have permission to use that command");
        return true; // merrily assumes shit's been handled appropriately
    }

    private boolean handleStake(CommandSender sender) {
        // find if the player has a claim in the current world or not
        PlayerClaim claim = plugin.loadClaim((Player)sender);
        List<String> denyworld = plugin.getConfig().getStringList("claim.deny-in-worlds");
        if(claim != null && claim.existsInWorld(((Player)sender).getLocation().getWorld())) {
            plugin.sendMessage(sender, "&cYou already have a claim in this world...");
            return true;
        } 
        if(denyworld.contains(((Player)sender).getLocation().getWorld().getName())) {
            plugin.sendMessage(sender, "&cYou can not claim any areas in this world...");
            return true;
        }
        if(claim.createInWorld(((Player)sender).getLocation())) {
            plugin.sendMessage(sender, "&7A " + ((plugin.getConfig().getInt("claim.size.x") * 2) + 1) + " by " + ((plugin.getConfig().getInt("claim.size.z") * 2) + 1) + " claim has been created centered on you.");
            plugin.sendMessage(sender, "&7You can now use &f/claim set help&7 to view flags that you can set");
        } else {
            plugin.sendMessage(sender, "&cCould not create a claim, probably means you are too close to someone else...");
        }
        return true;
    }

    private boolean handleForget(CommandSender sender) {
        PlayerClaim claim = plugin.loadClaim((Player)sender);
        if(claim != null && claim.existsInWorld(((Player)sender).getLocation().getWorld())) {
            claim.removeFromWorld(((Player)sender).getLocation().getWorld());
            plugin.sendMessage(sender, "&7Your claim has been removed");
        } else {
            plugin.sendMessage(sender, "&cYou have no claim in this world...");
        } 
        return true;
    }

    private void handleSetHelp(CommandSender sender) {
        plugin.sendMessage(sender, "&7Set flags using &f/claim set <flag> <value> &7where value is either \"allow\" or \"deny\"");
        plugin.sendMessage(sender, "&7Set your claim name using &f/claim set name <name of your claim goes here>");
        plugin.sendMessage(sender, "&7You can set the following flags on your claim:");
        List<String> enabled = plugin.getConfig().getStringList("flags.enabled");
        String e = "";
        for(String p : enabled) {
            e = e + p + ", ";
        }
        plugin.sendMessage(sender, "&7" + e + "name");
    }

    private boolean handleInfo(CommandSender sender, String[] args) {
        PlayerClaim claim = plugin.loadClaim((Player)sender);
        if(claim != null && claim.existsInWorld(((Player)sender).getLocation().getWorld())) {
            ProtectedRegion region = claim.getClaimRegion();
            boolean hasFlags = false;

            plugin.sendMessage(sender, "&7Claim center location: &f" + claim.getCenterCoordinates(((Player)sender).getLocation().getWorld()));
            DefaultDomain members = region.getMembers();
            if(members.size() != 0) {
                plugin.sendMessage(sender, "&7Members: &f" + members.toUserFriendlyString());
            } else {
                plugin.sendMessage(sender, "&7Members: &fnone");
            } 
            plugin.sendMessage(sender, "&7Flags:");
            plugin.sendMessage(sender, "&7--------------------------------------");
            List<String> enabled = plugin.getConfig().getStringList("flags.enabled");
            for(Flag<?> flag : DefaultFlag.getFlags()) {
                if(!enabled.contains(flag.getName())) continue;
                if(flag.getName().equalsIgnoreCase("name")) continue;
                Object val = region.getFlag(flag), group = null;
                if(val == null) continue;
                plugin.sendMessage(sender, "&7" + flag.getName() + ": &f" + String.valueOf(val) + "&7");
            }
        } else {
            plugin.sendMessage(sender, "&cYou have no claim in this world...");
        }
        return true;
    }

    private boolean handleAdd(CommandSender sender, String[] args) {
        PlayerClaim claim = plugin.loadClaim((Player)sender);
        if(claim != null && claim.existsInWorld(((Player)sender).getLocation().getWorld())) {
            if(args.length > 1) {
                claim.addMember(sender, args[1]);
                plugin.sendMessage(sender, "&f" + args[1] + "&7 added to member list");
            } else {
                plugin.sendMessage(sender, "&cUsage: /claim add <playername>");
            }
        } else {
            plugin.sendMessage(sender, "&cYou have no claim in this world...");
        }
        return true;
    }

    private boolean handleRemove(CommandSender sender, String[] args) {
        PlayerClaim claim = plugin.loadClaim((Player)sender);
        if(claim != null && claim.existsInWorld(((Player)sender).getLocation().getWorld())) {
            if(args.length > 1) {
                claim.removeMember(sender, args[1]);
                plugin.sendMessage(sender, "&f" + args[1] + "&7 removed from member list");
            } else {
                plugin.sendMessage(sender, "&cUsage: /claim remove <playername>");
            }
        } else {
            plugin.sendMessage(sender, "&cYou have no claim in this world...");
        }
        return true;
    }

    private boolean handleSet(CommandSender sender, String[] args) {
        PlayerClaim claim = plugin.loadClaim((Player)sender);
        if(claim != null && claim.existsInWorld(((Player)sender).getLocation().getWorld())) {
            if(args.length > 1) {
                if(args[1].equalsIgnoreCase("help")) {
                    handleSetHelp(sender);
                } else {
                    // see if any of these are enabled
                    List<String> enabled = plugin.getConfig().getStringList("flags.enabled");
                    if(enabled.contains(args[1].toLowerCase())) {
                        if("name".equalsIgnoreCase(args[1])) {
                            String name = "";
                            for(int i = 2; i < args.length; i++) {
                                name = name + args[i] + " ";
                            }
                            if(name.length() > 5 && name.length() < 40) {
                                String greeting = ChatColor.GRAY + "Entering: " + ChatColor.WHITE + name + ChatColor.GRAY + " (" + ChatColor.WHITE + ((Player)sender).getName() + ChatColor.GRAY + ")";
                                String farewell = ChatColor.GRAY + "Leaving: " + ChatColor.WHITE + name + ChatColor.GRAY + " (" + ChatColor.WHITE + ((Player)sender).getName() + ChatColor.GRAY + ")";
                                claim.setRegionFlag(sender, "greeting", greeting);
                                claim.setRegionFlag(sender, "farewell", farewell);
                            } else {
                                plugin.sendMessage(sender, "&cYou must give a name between 5 and 40 characters long");
                            }
                        } else {
                            Flag<?> setFlag = claim.fuzzyMatchFlag(args[1].toLowerCase());
                            if(setFlag == null) {
                                handleSetHelp(sender);
                            } else {
                                ProtectedRegion r = claim.getClaimRegion();
                                if(args[2] != null) {
                                    if(claim.setRegionFlag(sender, setFlag, args[2])) {
                                        plugin.sendMessage(sender, "&7Flag &f" + setFlag.getName() + "&7 has been updated...");
                                    } else {
                                        plugin.sendMessage(sender, "&cFlag " + setFlag.getName() + " could not be updated!");
                                    }
                                } else {
                                    handleSetHelp(sender);
                                }
                            }
                        }
                    } else {
                        plugin.sendMessage(sender, "&cThat flag is not enabled, valid flags are as follows:");
                        String e = "";
                        for(String p : enabled) {
                            e = e + p + ", ";
                        }
                        plugin.sendMessage(sender, "&c" + e + "name");
                    }
                }
            } else {
                handleSetHelp(sender);
            }
        } else {
            plugin.sendMessage(sender, "&cYou have no claim in this world...");
        } 
        return true;
    }

    private boolean claimHelp(CommandSender sender) {
        plugin.sendMessage(sender, "&f/claim stake&7 to stake a claim in your current world");
        plugin.sendMessage(sender, "&f/claim forget&7 to forget an earlier staked claim in your current world");
        plugin.sendMessage(sender, "&f/claim set&7 to set claim options, use &f/claim set help&7 for full list");
        plugin.sendMessage(sender, "&f/claim add <playername>&7 to add allowed people to the build list");
        plugin.sendMessage(sender, "&f/claim remove <playername>&7 to remove allowed people from the build list");
        return true;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("claim")) {
            if(!sender.hasPermission("claim.use")) return noPermission(sender);

            /* the following commands are only for players */
            if(sender instanceof Player) {
                if(args.length > 0) {
                    if("stake".equalsIgnoreCase(args[0])) {
                        if(!sender.hasPermission("claim.use.stake")) return noPermission(sender);
                        return handleStake(sender);
                    } else if("forget".equalsIgnoreCase(args[0])) {
                        if(!sender.hasPermission("claim.use.forget")) return noPermission(sender);
                        return handleForget(sender);
                    } else if("set".equalsIgnoreCase(args[0])) {
                        if(!sender.hasPermission("claim.use.set")) return noPermission(sender);
                        return handleSet(sender, args);
                    } else if("info".equalsIgnoreCase(args[0])) {
                        if(!sender.hasPermission("claim.use.info")) return noPermission(sender);
                        return handleInfo(sender, args);
                    } else if("add".equalsIgnoreCase(args[0])) {
                        if(!sender.hasPermission("claim.use.add")) return noPermission(sender);
                        return handleAdd(sender, args);
                    } else if("remove".equalsIgnoreCase(args[0])) {
                        if(!sender.hasPermission("claim.use.remove")) return noPermission(sender);
                        return handleRemove(sender, args);
                    } else if("help".equalsIgnoreCase(args[0])) {
                        return claimHelp(sender);
                    } else {
                        return claimHelp(sender);
                    }
                } else {
                    return claimHelp(sender);
                }
            }
            return true;
        }
        return false;
    }
}
